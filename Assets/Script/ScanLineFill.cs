﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class ScanLineFill : MonoBehaviour
{
    public Texture2D targetTex;
    public Gradient gradient;

    public enum DrawingColor
    {
        Red,
        Blue,
        Green,
        Yellow
    }

    DrawingColor currentDrawingColor = DrawingColor.Red;
    Color color;

    [System.Serializable]
    public class EdgeBucket
    {
        public int yMax = 0;
        public int yMin = 0;
        public int xMax = 0;
        public int xMin = 0;
        public float sign = 0;
        public float dX = 0;
        public float dY = 0;
        public int sum = 0;
    }

    public List<EdgeBucket> EdgeTable = new List<EdgeBucket>();
    public List<EdgeBucket> ActiveList = new List<EdgeBucket>();

    public void Clear()
    {
        EdgeTable.Clear();
    }
       
    public void AddEdge(int x1, int y1, int x2, int y2)
    {
        EdgeBucket edgeBucket = new EdgeBucket();

        if (y1 > y2)
        {
            edgeBucket.yMax = y1;
            edgeBucket.xMax = x1;

            edgeBucket.yMin = y2;
            edgeBucket.xMin = x2;
        }
        else
        {
            edgeBucket.yMax = y2;
            edgeBucket.xMax = x2;

            edgeBucket.yMin = y1;
            edgeBucket.xMin = x1;
        }

        edgeBucket.sum = x1;

        float dx = (float)x2 - (float)x1;
        float dy = (float)y2 - (float)y1;

        float m = (float)dy / (float)dx;

        if (m < 0)
        {
            edgeBucket.sign = -1;
        }
        else
        {
            edgeBucket.sign = 1;
        }

        edgeBucket.dX = Mathf.Abs(dx);
        edgeBucket.dY = Mathf.Abs(dy);

        if (Mathf.Abs(edgeBucket.dY) > 0)
        {
            this.EdgeTable.Add(edgeBucket);
        }
    }


    public void ProcessEdgeTable()
    {
        if (EdgeTable.Count <= 0)
            return;

        EdgeTable.Sort((a, b) => a.yMin.CompareTo(b.yMin));

        int y = EdgeTable[0].yMin;

        int minX = int.MaxValue;
        int maxX = int.MinValue;
        for (int itET = 0; itET < EdgeTable.Count; itET++)
        {
            EdgeBucket bucket = EdgeTable[itET];

            if (bucket.xMin <= minX)
                minX = bucket.xMin;

            if (bucket.xMax <= minX)
                minX = bucket.xMax;

            if (bucket.xMax >= maxX)
                maxX = bucket.xMax;

            if (bucket.xMin >= maxX)
                maxX = bucket.xMin;
        }

        while (EdgeTable.Count > 0)
        {
            for (int itET = 0; itET < EdgeTable.Count; itET++)
            {
                EdgeBucket currentBucket = EdgeTable[itET];

                if (currentBucket.yMin == y && currentBucket.dY != 0)
                {
                    ActiveList.Add(currentBucket);
                }
            }

            ActiveList.Sort((a, b) => a.xMin.CompareTo(b.xMin));

            for (int itAL = 0; itAL < ActiveList.Count; itAL++)
            {
                EdgeBucket currentBucket = ActiveList[itAL];

                if (currentBucket.dX != 0)
                {
                    float calcX = (float)(y - currentBucket.yMin) / (currentBucket.dY / currentBucket.dX);

                    currentBucket.sum = (int)(currentBucket.xMin + currentBucket.sign * calcX);
                }
            }

            List<Vector2Int> edgePair = new List<Vector2Int>();
            for (int x = minX; x <= maxX; x++)
            {
                for (int i = 0; i < ActiveList.Count; i++)
                {
                    EdgeBucket activeBucket = ActiveList[i];
                    if (activeBucket.sum == x)
                    {
                        edgePair.Add(new Vector2Int(x, y));
                    }


                    if (edgePair.Count >= 2)
                    {
                        var pair1 = edgePair[0];
                        var pair2 = edgePair[1];

                        for (int itPair = pair1.x; itPair <= pair2.x; itPair++)
                        {
                            switch(this.currentDrawingColor)
                            {
                                case DrawingColor.Red:
                                    color = Color.red;
                                    break;
                                case DrawingColor.Blue:
                                    color = Color.blue;
                                    break;
                                case DrawingColor.Green:
                                    color = Color.green;
                                    break;
                                case DrawingColor.Yellow:
                                    color = Color.yellow;
                                    break;

                            }
                            //Color color = gradient.Evaluate(Mathf.InverseLerp(minX, maxX, y));
                            //Color newColor = color + previousColor * (1f - color.a);
                            targetTex.SetPixel(itPair, y, color);
                        }
                        edgePair.Clear();
                    }
                }

            }

            y++;

            for (int itAL = ActiveList.Count - 1; itAL >= 0; itAL--)
            {
                EdgeBucket activeEdge = ActiveList[itAL];
                if (y == activeEdge.yMax)
                {
                    EdgeTable.Remove(activeEdge);
                    ActiveList.Remove(activeEdge);
                }
            }
        }
    }

    public void SetCurrentDrawingColor(string drawingColor)
    {
        switch (drawingColor.ToLower())
        {
            case "red":
                this.currentDrawingColor = DrawingColor.Red;
                break;
            case "blue":
                this.currentDrawingColor = DrawingColor.Blue;
                break;
            case "green":
                this.currentDrawingColor = DrawingColor.Green;
                break;
            case "yellow":
                this.currentDrawingColor = DrawingColor.Yellow;
                break;
        }
    }

}